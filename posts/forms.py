from .models import Posts, Comments
from django import forms

class PostsModelForm(forms.ModelForm):

    class Meta:
        model = Posts
        fields = ("post",)

class CommentModelForm(forms.ModelForm):

    class Meta:
        model = Comments
        fields = ("comment",)