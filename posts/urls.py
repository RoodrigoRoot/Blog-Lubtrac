from django.urls import path
from .views import IndexView, PostCreateView, PostUpdateView, PostDeleteView, CommentsPostView

urlpatterns = [
    path("", IndexView.as_view(), name="index"),
    path("entrada/crear/", PostCreateView.as_view(), name="post_create"),
    path("entrada/actualizar/<int:pk>/", PostUpdateView.as_view(), name="posts_update"),
    path("entrada/eliminar/<int:pk>/", PostDeleteView.as_view(), name="posts_delete"),
    path("entrada/eliminar/<int:pk>/comments/", CommentsPostView.as_view(), name="comments"),

]
