from django.db import models
from django.contrib.auth.models import User
# Create your models here.

class Posts(models.Model):
    post = models.CharField(verbose_name="Entrada", max_length=150)
    author = models.ForeignKey(User, verbose_name="Creador", on_delete=models.CASCADE)
    created_at = models.DateTimeField(verbose_name="Creación", auto_now=False, auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Creación", auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.author.username
    
    class Meta:
        verbose_name = "Entrada"
        verbose_name_plural = "Entradas"

class Comments(models.Model):
    
    post = models.ForeignKey(Posts, verbose_name="Entrada", on_delete=models.CASCADE)
    author = models.ForeignKey(User, verbose_name="Creador", on_delete=models.CASCADE)
    comment = models.TextField(verbose_name="Comentario")
    created_at = models.DateTimeField(verbose_name="Creación", auto_now=False, auto_now_add=True)
    updated_at = models.DateTimeField(verbose_name="Creación", auto_now=True, auto_now_add=False)

    def __str__(self):
        return self.author.username
    
    class Meta:
        verbose_name = "Comentario"
        verbose_name_plural = "comentarios"