from django.shortcuts import render, redirect, reverse
from django.views import View
from django.views.generic import CreateView, UpdateView, DeleteView
from django.contrib.auth.mixins import LoginRequiredMixin
from .models import Posts
from .forms import *
# Create your views here.

class IndexView(View):
    def get(self, request, *args, **kwargs):
        posts = Posts.objects.all().order_by("-post")
        return render(request, 'posts/index.html', locals())


class PostCreateView(CreateView, LoginRequiredMixin):
    model = Posts
    template_name = "posts/create_posts.html"
    success_url = "/"
    form_class = PostsModelForm

    def post(self, request, *args, **kwargs):
        form = PostsModelForm(data=request.POST)
        if form.is_valid():
            post = Posts.objects.create(
                post=form.cleaned_data["post"],
                author=request.user
            )
            if post:
                return redirect(reverse("index"))
        return render(request, "posts/create_posts.html", locals())
        


class PostUpdateView(UpdateView, LoginRequiredMixin):
    model = Posts
    template_name = "posts/update_posts.html" 
    success_url = "/"
    form_class = PostsModelForm


class PostDeleteView(DeleteView, LoginRequiredMixin):
    model = Posts
    template_name = "posts/delete_posts.html"
    success_url = "/"
    form_class = PostsModelForm


class CommentsPostView(View, LoginRequiredMixin):

    def get(self, request, *args, **kwargs):
        comments = Comments.objects.filter(post=self.kwargs["pk"])
        post = Posts.objects.get(id=self.kwargs["pk"])

        return render(request, 'posts/comments.html', locals())