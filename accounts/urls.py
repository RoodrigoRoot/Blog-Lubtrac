from django.urls import path
from .views import LoginView, UserCreateView, UserUpdateView, logout_view

urlpatterns = [
    path("entrar/", LoginView.as_view(), name="login"),
    path("salir/", logout_view, name="logout"),
    path("registrar/", UserCreateView.as_view(), name="register_user"),
    path("cuenta/actualizar/<int:pk>/", UserUpdateView.as_view(), name="update_user"),
]
