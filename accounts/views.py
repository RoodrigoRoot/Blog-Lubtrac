from django.shortcuts import render, redirect, reverse
from django.contrib.auth.forms import AuthenticationForm
from django.views import View
from django.views.generic import CreateView, UpdateView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.forms import UserCreationForm
from django.contrib.auth.models import User
# Create your views here.

class LoginView(View):

    def get(self, request, *args, **kwargs):
        form = AuthenticationForm()
        return render(request, 'login.html', locals())
    
    def post(self, request, *args, **kwargs):
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            username = request.POST['username']
            password = request.POST['password']
            user = authenticate(request, username=username, password=password)
            if user is not None:
                login(request, user)
                return redirect("/")
        return render(request, 'login.html', locals())


class UserCreateView(CreateView):
    model = User
    form_class = UserCreationForm
    template_name = "register.html"
    success_url = "/"


class UserUpdateView(UpdateView, LoginRequiredMixin):
    model = User
    form_class = UserCreationForm
    template_name = "accounts/update_user.html"
    success_url = "/"


def logout_view(request):
    logout(request)
    return redirect(reverse("login"))