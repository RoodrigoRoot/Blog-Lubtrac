FROM python:3.6

ENV PYTHONUNBUFFERED=1

RUN mkdir blog
WORKDIR  blog/

RUN mkdir /var/log/blog
RUN chmod 777 /var/log/blog

COPY requirements.txt .
RUN pip install -r requirements.txt

COPY . .
